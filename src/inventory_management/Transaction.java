/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

public class Transaction {
	private Item item;
	private String transactionID = "";
	private String borrowDate = "";
	private boolean toBeReturned = false;
	private String returnDate = "";	//if reusable/not consumed
	private String uploaded = "";

	public Transaction(Item item) {
		this.item = item;
		this.transactionID = item.getRFIDCode()+Records.getNow();
		if(this.item.getReusablility()){
			toBeReturned = true;
		}
		this.borrowDate = Records.getNow();
		this.uploaded = "99999999999999"; //according to the format yyyyMMddHHmmss
	}
	
	private Transaction(Item item, String transactionID, String borrowDate,String returnDate, String uploaded){
		this.item = item;
		this.transactionID = transactionID;
		if(this.item.getReusablility()){
			toBeReturned = true;
		}
		this.borrowDate = borrowDate;
		this.returnDate = returnDate;
		this.uploaded = uploaded;
	}
	
	private Transaction(Item item, String transactionID, String borrowDate, String uploaded){
		this.item = item;
		this.transactionID = transactionID;
		if(this.item.getReusablility()){
			toBeReturned = true;
		}
		this.borrowDate = borrowDate;
		this.uploaded = uploaded;
	}
	
	public String toString(){
		return item.getRFIDCode() +" "+ this.transactionID + " " + this.borrowDate + " " + this.returnDate +" " + this.uploaded;
	}

	public static Transaction fromString(String string){
		if(ItemsDB.getItemByRFIDCode(string.split(" ")[0])==null){
			return null;
		}
		try{
			if(string.split(" ").length==5){
				return new Transaction(ItemsDB.getItemByRFIDCode(string.split(" ")[0]),string.split(" ")[1],string.split(" ")[2],string.split(" ")[3],string.split(" ")[4]);
			}else
				return new Transaction(ItemsDB.getItemByRFIDCode(string.split(" ")[0]),string.split(" ")[1],string.split(" ")[2],string.split(" ")[3]);
		}catch(ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * @returns
	 * <tt>true<tt/> if item is successfully returned
	 * <tt>false<tt/> if item is not reusable
	 */
	public boolean returnItem(){
		if(!toBeReturned){
			return false;
		}
		returnDate = Records.getNow();
		toBeReturned = false;
		return true;
	}
	
	public boolean equals(Object o){
		if(o.getClass().toString().compareTo(this.getClass().toString())==0){
			Transaction temp = (Transaction)o;
			if(		(temp.item.getRFIDCode().equalsIgnoreCase(this.item.getRFIDCode()))&&
					(temp.borrowDate.equalsIgnoreCase(this.borrowDate))&&
					(temp.returnDate.equalsIgnoreCase(this.returnDate))){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param transaction
	 * the transaction to check if it is a updated version of
	 * 
	 * @returns
	 * -1 if different transaction<br/>
	 * 0 if exactly the same<br/>
	 * 1 if transaction object precedes the argument transaction<br/>
	 * 2 if transaction follows the argument transaction
	 */
	public int updatedTransaction(Transaction transaction){
		if(this.transactionID.compareTo(transaction.getTransactionID())==0){
			//if both return dates are the same they are equal
			if(((this.returnDate.isEmpty())&&(transaction.getReturnDate().isEmpty()))||
			(this.returnDate.compareTo(transaction.getReturnDate())==0)){
				return 0;
			}
			//if only one has a return date that one follows
			else if((this.returnDate.isEmpty())&&(!transaction.getReturnDate().isEmpty())){
				return 1;
			}else if((!this.returnDate.isEmpty())&&(transaction.getReturnDate().isEmpty())){
				return 2;
			}
			// if both have different return dates, take the earlier one
			else{
				if(this.returnDate.compareTo(transaction.getReturnDate())<0){
					return 1;
				}else{
					return 2;
				}
			}
		}else{
			return -1;
		}
	}
	/**
	 * @return
	 * the RFID Code of the item involved in the transaction
	 */
	public String getItemRFIDCode(){
		return item.getRFIDCode();
	}
	
	public Item getItem(){
		return this.item;
	}
	
	public String getBorrowDate(){
		return borrowDate;
	}
	
	public String getReturnDate(){
		return returnDate;
	}
	
	public String getUploaded(){
		return uploaded;
	}

	public String getTransactionID(){
		return transactionID;
	}
	
	/**
	 * @return
	 * return true if the item is reusable and is not yet returned
	 */
	public boolean toBeReturned(){
		return toBeReturned;
	}
	
	public void setUploaded(String uploaded){
		this.uploaded = uploaded;
	}
	

}
