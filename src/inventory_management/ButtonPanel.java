/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

class ButtonPanel extends JPanel implements ActionListener{

	JButton scanItem = null;
	JButton viewItemsOnLoan = null;
	JButton viewItemsInInventory = null;
	JButton viewItem = null;
	JButton addNewItemToInventory = null;
	JButton removeItemFromInventory = null;
	JButton uploadRecords = null;
	JButton downloadRecords = null;
	JButton exitSystem = null;
	final private String SCAN_ITEM = "Scan Item";
	final private String VIEW_ITEMS_ON_LOAN = "View Items On loan";
	final private String VIEW_ITEMS_IN_INVENTORY = "View Items in inventory";
	final private String VIEW_ITEM = "View Item";
	final private String ADD_NEW_ITEM_TO_INVENTORY = "Add New Item to inventory";
	final private String REMOVE_ITEM_FROM_INVENTORY = "Remove Item from inventory"; 
	final private String UPLOAD_RECORDS = "Upload records";
	final private String DOWNLOAD_RECORDS = "Download records"; 
	final private String EXIT_SYSTEM = "Exit System";
	
	public ButtonPanel(){
		scanItem = new JButton(SCAN_ITEM);
		viewItemsOnLoan = new JButton(VIEW_ITEMS_ON_LOAN);
		viewItemsInInventory = new JButton(VIEW_ITEMS_IN_INVENTORY);
		viewItem = new JButton(VIEW_ITEM);
		addNewItemToInventory = new JButton(ADD_NEW_ITEM_TO_INVENTORY);
		removeItemFromInventory = new JButton(REMOVE_ITEM_FROM_INVENTORY);
		uploadRecords = new JButton(UPLOAD_RECORDS);
		downloadRecords = new JButton(DOWNLOAD_RECORDS);
		exitSystem = new JButton(EXIT_SYSTEM);
		
		setLayout(new GridLayout(0,1));//rows,columns (0 means any number)
		
		scanItem.addActionListener(this);
		viewItemsOnLoan.addActionListener(this);
		viewItemsInInventory.addActionListener(this);
		viewItem.addActionListener(this);
		addNewItemToInventory.addActionListener(this);
		removeItemFromInventory.addActionListener(this);
		uploadRecords.addActionListener(this);
		downloadRecords.addActionListener(this);
		exitSystem.addActionListener(this);
		
		add(scanItem);
		add(viewItemsOnLoan);
		add(viewItemsInInventory);
		add(viewItem);
		add(addNewItemToInventory);
		add(removeItemFromInventory);
		add(uploadRecords);
		add(downloadRecords);
		add(exitSystem);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try{
		if(e.getActionCommand().equalsIgnoreCase(SCAN_ITEM)){
			ItemsDB.waitingRead = 1;
			System.out.println("Waiting for RFID Input");
			Menu.wait = true;
			synchronized(ItemsDB.lock){
				while(Menu.wait){
					ItemsDB.lock.wait();
				}
			}
		}else if(e.getActionCommand().equalsIgnoreCase(VIEW_ITEMS_ON_LOAN)){
			ArrayList<Item> temp = ItemsDB.viewItemsOnLoan();
			for(int count = 0; count <temp.size();count++){
				MainPanel.addItem(temp.get(count));
			}
		}else if(e.getActionCommand().equalsIgnoreCase(VIEW_ITEMS_IN_INVENTORY)){
			ArrayList<Item> temp = ItemsDB.viewItemsInInventory();
			for(int count = 0; count <temp.size();count++){
				MainPanel.addItem(temp.get(count));
			}
			
		}else if(e.getActionCommand().equalsIgnoreCase(VIEW_ITEM)){
			ItemsDB.waitingRead = 4;
			System.out.println("Waiting for RFID Input");
			Menu.wait = true;
			synchronized(ItemsDB.lock){
				while(Menu.wait){
					ItemsDB.lock.wait();
				}
			}
		}else if(e.getActionCommand().equalsIgnoreCase(ADD_NEW_ITEM_TO_INVENTORY)){
			ItemsDB.waitingRead = 5;
			System.out.println("Waiting for RFID Input");
			Menu.wait = true;
			synchronized(ItemsDB.lock){
				while(Menu.wait){
					ItemsDB.lock.wait();
				}
			}
		}else if(e.getActionCommand().equalsIgnoreCase(REMOVE_ITEM_FROM_INVENTORY)){
			ItemsDB.waitingRead = 6;
			System.out.println("Waiting for RFID Input");
			Menu.wait = true;
			synchronized(ItemsDB.lock){
				while(Menu.wait){
					this.wait();
				}
			}
		}else if(e.getActionCommand().equalsIgnoreCase(UPLOAD_RECORDS)){
			if(Menu.itemsDB.uploadDB()){
				System.out.println("update successful");
			}else{
				System.out.println("update unsuccessful");
			}
		}else if(e.getActionCommand().equalsIgnoreCase(DOWNLOAD_RECORDS)){
			if(Menu.itemsDB.downloadDB()){
				System.out.println("download successful");
			}else{
				System.out.println("download unsuccessful");
			}
		}else if(e.getActionCommand().equalsIgnoreCase(EXIT_SYSTEM)){
			Menu.exit();
		}else{
			
		}
		}catch(InterruptedException e1){
			System.out.println("ButtonPanel Interrupted");
		}
	}
	
}