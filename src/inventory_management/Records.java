/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Records {

	private static SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
	private static File transactionRecords,inventoryRecords;
	private static BufferedWriter trbw = null,irbw = null;//transactionRecords/inventoryRecords buffered writer
	private static BufferedReader trbr = null,irbr = null;//transactionRecords/inventoryRecords buffered reader
	private ItemsDB itemsDB = null;


	public Records(ItemsDB itemsDB) {
		this.itemsDB = itemsDB;
		inventoryRecords = new File("InventoryRecords");
		transactionRecords = new File("TransactionRecords");
		try {
			//for hashing/data security not yet implemented
			//md = MessageDigest.getInstance("SHA-512", "BC"); 
			if(!transactionRecords.exists()){
				transactionRecords.createNewFile();
				trbw = new BufferedWriter(new FileWriter(transactionRecords));
				trbw.write("0\n");//initialises a 0 item records
				trbw.flush();
			}else{
				trbw = new BufferedWriter(new FileWriter(transactionRecords,true));
			}
			trbr = new BufferedReader(new FileReader(transactionRecords));
			if(!inventoryRecords.exists()){
				inventoryRecords.createNewFile();
				irbw = new BufferedWriter(new FileWriter(inventoryRecords));
				irbw.write("0\n");//initialises a 0 item inventory
				irbw.flush();
			}else{
				irbw = new BufferedWriter(new FileWriter(inventoryRecords,true));
			}
			irbr = new BufferedReader(new FileReader(inventoryRecords));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * write record to file
	 */
	public void writeToFile(String towrite){
		try {
			trbw.write(format.format(new Date())+":");
			trbw.write(towrite);
			trbw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Initialises the DB from the records if they exist
	 */
	public ArrayList<Item> initialiseItemsDB(ArrayList<Item> itemsDB){
		try{
			ItemsDB.numOfInventory = Integer.parseInt(irbr.readLine());
		}catch(NumberFormatException e){
			e.printStackTrace();
			ItemsDB.numOfInventory = 0;
		}catch(IOException e){
			e.printStackTrace();
			ItemsDB.numOfInventory = 0;
		}
		try{
			for(int count = 0 ; count < ItemsDB.numOfInventory ; count++){
				ItemsDB.addItem(Item.fromString(irbr.readLine()));
			}
		}catch(IOException e){
			e.printStackTrace();
			return itemsDB;
		}
		return itemsDB;
	}

	/* 
	 * initialises the transaction records from the inventory records
	 */
	public ArrayList<Transaction> initialiseTransactionRecords(ArrayList<Transaction> transactionList){
		int numOfTransaction = 0;
		try{
			numOfTransaction = Integer.parseInt(trbr.readLine());
		}catch(NumberFormatException e){
			e.printStackTrace();
			numOfTransaction = 0;
		}catch(IOException e){
			e.printStackTrace();
			numOfTransaction = 0;
		}
		try{
			for(int count = 0 ; count < numOfTransaction ; count++){
				ItemsDB.addTransaction(Transaction.fromString(trbr.readLine()));
			}
		}catch(IOException e){
			e.printStackTrace();
			return transactionList;
		}
		return transactionList;
	}

	//TODO
	/*
	 * Reads records from another file
	 */
	public boolean readRecords(){
		File newInventoryRecords,newTransactionRecords;
		BufferedReader nirbr = null,ntrbr = null;
		Item tempItem;
		Transaction tempTransaction;
		int size;
		try{
			File tempFile = new File(".");
			String[] fileList = tempFile.list();
			for(String fileName : fileList){
				if(fileName.startsWith("InventoryRecords")){
					newInventoryRecords = new File(fileName);
					nirbr = new BufferedReader(new FileReader(newInventoryRecords));
					try{
						size = Integer.parseInt(nirbr.readLine());
						for(int count = 0; count < size;count++){
							tempItem = Item.fromString(nirbr.readLine());
							
							//If item is not alr inside the itemDB
							if(ItemsDB.getItemByRFIDCode(tempItem.getRFIDCode())==null){
								itemsDB.addNewItem(tempItem);
							}
						}
					}catch(NumberFormatException e){
						e.printStackTrace();
						ItemsDB.numOfInventory = 0;
					}catch(IOException e){
						e.printStackTrace();
						ItemsDB.numOfInventory = 0;
					}
				}else if(fileName.startsWith("TransactionRecords")){
					newTransactionRecords = new File(fileName);
					ntrbr = new BufferedReader(new FileReader(newTransactionRecords));
					try{
						size = Integer.parseInt(ntrbr.readLine());
						for(int count = 0; count < size;count++){
							tempTransaction = Transaction.fromString(ntrbr.readLine());
							
							//If transaction is not alr inside the transactionDB
							if(ItemsDB.containsTransaction(tempTransaction.getTransactionID())==null){
								ItemsDB.getTransactionDB().add(tempTransaction);
							}//TODO updated transaction
							//case of outdated transactions?
						}
					}catch(NumberFormatException e){
						e.printStackTrace();
						ItemsDB.numOfInventory = 0;
					}catch(IOException e){
						e.printStackTrace();
						ItemsDB.numOfInventory = 0;
					}
				}
			}
		}catch(NullPointerException e){
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}finally{
			if(nirbr!=null){
				try {
					nirbr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(ntrbr!=null){
				try {
					ntrbr.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	/*
	 * To print the data to the files before closing
	 */
	private boolean closeInventoryRecord(){
		try{
			inventoryRecords.createNewFile();
			irbw = new BufferedWriter(new FileWriter(inventoryRecords));
			irbw.write(ItemsDB.getItemDB().size()+"\n");
			for(int count = 0 ; count < ItemsDB.getItemDB().size(); count++){
				irbw.write(ItemsDB.getItemDB().get(count).toString()+"\n");
				irbw.flush();
			}
		}catch(IOException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean closeTransactionRecord(){
		try{
			transactionRecords.createNewFile();
			trbw = new BufferedWriter(new FileWriter(transactionRecords));
			trbw.write(ItemsDB.getTransactionDB().size()+"\n");
			for(int count = 0 ; count < ItemsDB.getTransactionDB().size(); count++){
				trbw.write(ItemsDB.getTransactionDB().get(count).toString()+"\n");
				trbw.flush();
			}
		}catch(IOException e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean closeRecords(){
		if(!closeInventoryRecord()){
			System.out.println("Error closing inventory Records.");
		}
		if(!closeTransactionRecord()){
			System.out.println("Error closing transaction Records.");
		}
		if(trbw!=null){
			try {
				trbw.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		if(trbr!=null){
			try {
				trbr.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		if(irbw!=null){
			try {
				irbw.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		if(irbr!=null){
			try {
				irbr.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	/*
	 * returns the date as a string based on the format yyyyMMddHHmmss
	 */
	public static String getNow(){
		return format.format(new Date());
	}
	
	public static String formatTimestamp(Timestamp timestamp){
		return format.format(timestamp);
	}

}
