/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JFrame;

public class Menu implements Runnable{

	static ItemsDB itemsDB = null;
	public static boolean wait = false;
	private static JFrame frame = null;
	private static MainPanel mainPanel;
	private static ButtonPanel buttonPanel;

	public Menu(ItemsDB itemsDB){
		Menu.itemsDB = itemsDB;
		/*
		frame = new JFrame();
		configureFrames();
		*/
	}

	public void run(){
		String temp;
		try{
			//String choice = "";
			char choice = ' ';
			while(true){
				synchronized(ItemsDB.lock){
					while(wait){
						ItemsDB.lock.wait();
					}
				}
				printMenu();
				try{
					temp = ItemsDB.sc.next();
					choice = temp.charAt(0);
				}catch(Exception e){
					//if not supposed to receive input
					if(wait){
						continue;
					}
					e.printStackTrace();
					System.out.println("choice:"+choice);
				}
				//if not supposed to receive input
				if(wait){
					continue;
				}
				switch(choice){
				/*
				 * Go into a wait for scan mode or press e to exit
				 */

				//case "1":	//scan item
				case '1':	//scan item
					ItemsDB.waitingRead = 1;
					System.out.println("Waiting for RFID Input");
					wait = true;
					synchronized(ItemsDB.lock){
						while(wait){
							ItemsDB.lock.wait();
						}
					}
					break;

					//case "2":	//view items on loan
				case '2':	//view items on loan
					ArrayList<Item> itemsOnLoan = ItemsDB.viewItemsOnLoan();
					System.out.println("===================================================");
					System.out.println("The items on loan are:");
					mainPanel.clearItems();
					for(int count = 0 ; count < itemsOnLoan.size() ; count++){
						MainPanel.addItem(itemsOnLoan.get(count));
						System.out.println(itemsOnLoan.get(count).toString());
					}
					System.out.println("===================================================");
					break;

					//case "3":	//view items in inventory
				case '3':	//view items in inventory
					ArrayList<Item> itemsInInventory = ItemsDB.viewItemsInInventory();
					System.out.println("===================================================");
					System.out.println("The items in inventory are:");
					mainPanel.clearItems();
					for(int count = 0 ; count < itemsInInventory.size() ; count++){
						MainPanel.addItem(itemsInInventory.get(count));
						System.out.println(itemsInInventory.get(count).toString());
					}
					System.out.println("===================================================");
					break;
					//case "4":	//view item
				case '4':	//view item
					ItemsDB.waitingRead = 4;
					System.out.println("Waiting for RFID Input");
					wait = true;
					synchronized(ItemsDB.lock){
						while(wait){
							ItemsDB.lock.wait();
						}
					}
					break;
					//case "5":	//add new item to inventory
				case '5':	//add new item to inventory
					ItemsDB.waitingRead = 5;
					System.out.println("Waiting for RFID Input");
					wait = true;
					synchronized(ItemsDB.lock){
						while(wait){
							ItemsDB.lock.wait();
						}
					}
					break;

					//case "6":	//Remove item from inventory
				case '6':	//Remove item from inventory
					ItemsDB.waitingRead = 6;
					System.out.println("Waiting for RFID Input");
					wait = true;
					synchronized(ItemsDB.lock){
						while(wait){
							this.wait();
						}
					}
					break;
					//case "7":	//upload records
				case '7':	//upload records
					if(itemsDB.uploadDB()){
						System.out.println("upload successful");
					}else{
						System.out.println("upload unsuccessful");
					}
					break;
					//case "8":	//download records
				case '8':	//download records
					if(itemsDB.downloadDB()){
						System.out.println("download successful");
					}else{
						System.out.println("download unsuccessful");
					}
					break;
				case 'r':
				case 'R':
					if(itemsDB.reconnect()){
						System.out.println("reconnect successful");
					}else{
						System.out.println("reconnect unsuccessful");
					}
					break;
					//case "e":
					//case "E":
				case 'e':
				case 'E':
					exit();
					break;
				default:
					System.out.println("Invalid Choice!");
					break;
					//debugging
				case 'z':
					System.out.println(Records.getNow());
					break;
				}
				mainPanel.printItems();
				choice = ' ';
			}
		}catch(InterruptedException e){
			System.out.println("Menu Interrupted");
		}
	}

	public static void printMenu(){
		System.out.print("" +
				"Welcome to RFID inventory system management\n" +
				"===================================================\n" +
				"1) Scan item(Check item in/out)\n" +
				"2) View items on loan\n" +
				"3) View items in inventory\n" +
				"4) View item\n" +
				"5) Add new item to inventory\n" +
				"6) Remove item from inventory\n" +
				"7) Upload records\n" +
				"8) Download Records\n" +
				"r) Reconnect to the Database\n" +
				"e) Exit the system\n" +
				"===================================================\n" +
				"Please enter your choice:");
	}

	private void configureFrames(){
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		buttonPanel = new ButtonPanel();
		mainPanel = new MainPanel();
		frame.getContentPane().add(buttonPanel, BorderLayout.WEST);
		frame.getContentPane().add(mainPanel, BorderLayout.CENTER);

		//size to be half of screensize
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize().width/2,Toolkit.getDefaultToolkit().getScreenSize().height/2 );
		//location to be in the middle of the screen
		frame.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/4, Toolkit.getDefaultToolkit().getScreenSize().height/4);
		//frame.setVisible(true);
		frame.setVisible(false);
	}

	public static void exit(){
		itemsDB.exit();
		if(frame!=null){
			WindowEvent wev = new WindowEvent(frame, WindowEvent.WINDOW_CLOSING);
			frame.dispatchEvent(wev);
			// this will hide and dispose the frame, so that the application quits by
			// itself if there is nothing else around. 
			frame.setVisible(false);
			frame.dispose();
		}
	}

}